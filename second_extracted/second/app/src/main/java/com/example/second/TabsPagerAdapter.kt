package com.example.second

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.gms.maps.SupportMapFragment


class TabsPagerAdapter(fm: FragmentManager, lifecycle: Lifecycle, private var numberOfTabs: Int) : FragmentStateAdapter(fm, lifecycle) {

    //val fragmentManager = fm
    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> {
                val bundle = Bundle()
                bundle.putString("fragmentName", "Map Fragment")
                val mapFragment = SupportMapFragment()
                mapFragment.getMapAsync(MapOptions())
                return mapFragment
            }
            1 -> {
                val bundle = Bundle()
                bundle.putString("fragmentName", "Route Fragment")
                val buildTheRouteFragment = BuildTheRouteFragment()
                buildTheRouteFragment.arguments = bundle
                return buildTheRouteFragment
            }
            2 -> {
                //val profileFragment = FAQTab()
                return FAQTab()
            }
            else -> return TabsFragment()
        }
    }

    override fun getItemCount(): Int {
        return numberOfTabs
    }
}