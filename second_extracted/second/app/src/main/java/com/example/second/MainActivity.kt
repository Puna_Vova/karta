package com.example.second

import android.annotation.SuppressLint
import android.app.Dialog
import android.view.View
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.os.Environment
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File

class MainActivity : AppCompatActivity() {

    private var userName = String()
    private var phoneNumber = String()
    private var path:File? = null
    private var file = File("C:\\download\\AndriodStudioProjects\\second\\UserData.txt")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //showFAQDialog(View(this))
        // Tabs Customization
        tab_layout.setSelectedTabIndicatorColor(Color.WHITE)
        //tab_layout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
        tab_layout.tabTextColors = ContextCompat.getColorStateList(this, android.R.color.white)

        // Set different Text Color for Tabs for when are selected or not
        //tab_layout.setTabTextColors(R.color.normalTabTextColor, R.color.selectedTabTextColor)

        // Number Of Tabs
        val numberOfTabs = 3

        // Set Tabs in the center
        //tab_layout.tabGravity = TabLayout.GRAVITY_CENTER

        // Show all Tabs in screen
        tab_layout.tabMode = TabLayout.MODE_FIXED

        // Scroll to see all Tabs
        //tab_layout.tabMode = TabLayout.MODE_SCROLLABLE

        // Set Tab icons next to the text, instead of above the text
        tab_layout.isInlineLabel = true

        // Set the ViewPager Adapter
        val adapter = TabsPagerAdapter(supportFragmentManager, lifecycle, numberOfTabs)
        tabs_viewpager.adapter = adapter

        // Enable Swipe
        tabs_viewpager.isUserInputEnabled = false

        // Link the TabLayout and the ViewPager2 together and Set Text & Icons
        TabLayoutMediator(tab_layout, tabs_viewpager) { tab, position ->
            when (position) {
                0 -> {
                    tab.text = "Map"
                    tab.setIcon(R.drawable.map)
                }
                1 -> {
                    tab.text = "Go"
                    tab.setIcon(R.drawable.go)

                }
                2 -> {
                    tab.text = "FAQ"
                    tab.setIcon(R.drawable.profile)
                }
            }
            // Change color of the icons
            tab.icon?.colorFilter =
                BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                    Color.WHITE,
                    BlendModeCompat.SRC_ATOP
                )
        }.attach()


        setCustomTabTitles()

        //showStartDialog()
        showUserDataInputDialog()
    }

    private fun setCustomTabTitles() {
        val vg = tab_layout.getChildAt(0) as ViewGroup
        val tabsCount = vg.childCount

        for (j in 0 until tabsCount) {
            val vgTab = vg.getChildAt(j) as ViewGroup

            val tabChildCount = vgTab.childCount

            for (i in 0 until tabChildCount) {
                val tabViewChild = vgTab.getChildAt(i)
                if (tabViewChild is TextView) {

                    // Change Font and Size
                    tabViewChild.typeface = Typeface.DEFAULT_BOLD
//                    val font = ResourcesCompat.getFont(this, R.font.myFont)
//                    tabViewChild.typeface = font
//                    tabViewChild.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25f)
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showUserDataInputDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.user_data_dialog)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )

        val submitButton = dialog.findViewById(R.id.submit_button) as Button
        submitButton.setOnClickListener {
//            val userName = dialog.findViewById<EditText>(R.id.userNameEditText)
//            val phoneNumber = dialog.findViewById<EditText>(R.id.phoneNumberEditText)
//            if (userName.text.isEmpty() || phoneNumber.text.isEmpty())
//                dialog.findViewById<TextView>(R.id.warningTextView).text = "Enter the data!"
//            else {
//                file.printWriter().use { out ->
//                    out.println(userName)
//                    out.println(phoneNumber)
//                }
//            }
            // here
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun showGreetingDialog(){
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
    }

    fun showFAQDialog(view: View){
        //FAQDialog().show(supportFragmentManager, "FAQ")
    }

    private fun showStartDialog(){
//        path = getExternalFilesDir(Environment.DIRECTORY_DCIM)
//        println(path)
//        file = File(path, "UserData.txt")
//        file.delete()
        if(file.createNewFile()) {
            showUserDataInputDialog()
        }
        else {
            val bufferedReader = file.bufferedReader()
            val data: List<String> = bufferedReader.readLines()
            userName = data[0]
            phoneNumber = data[1]
        }
    }
}