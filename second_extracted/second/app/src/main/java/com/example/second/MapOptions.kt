package com.example.second

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapOptions : OnMapReadyCallback {
    override fun onMapReady(googleMap: GoogleMap) {
        val map = googleMap
        val pp = LatLng(53.9, 27.56667)
        val option = MarkerOptions()
        option.position(pp).title("Bebra")
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(pp, 10f))
    }
}